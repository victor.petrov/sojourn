/** Victor Petrov
* victor_petrov@harvard.edu
* 40750991 
*/
package net.cs76.projects.student40750991;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLEncoder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import net.cs76.projects.student40750991.property.Property;
import net.cs76.projects.student40750991.property.PropertyAdapter;
import net.cs76.projects.student40750991.server.SojournServer;
import net.cs76.projects.student40750991.server.SojournServerException;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.SlidingDrawer;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

/** Main activity. Fetches a list of available Properties from the Sojourn
 * server and displays each one using a ListView. While the server request
 * is loading, a progress bar is displayed instead.
 * A search form can be revealed by clicking on a SlidingDrawer at the bottom
 * of the screen. The form causes the Property list to be refreshed with new
 * results from the Sojourn server.
 * When a Property is selected from the list, the ViewProperty activity is
 * called and the Property object is passed to it as a Parcelable object via
 * Intent extras. 
 */
public class SojournActivity extends Activity implements OnItemClickListener
{    
    private static final String HOST_URL = "http://emir-pro.appspot.com";
    private SojournServer _server;
    private ImageButton   _btnRetry;
    private ProgressBar   _pbLoading;
    private Property      _properties[];
    private ListView      _listProperties;
    private TextView      _txtNoResults;
    
    @Override
    /** Creates the layout from res/layout/list_properties.xml
     * Initiates the request to the Sojourn server
     * @see _connectToServer
     */
    public void onCreate(Bundle savedInstanceState)
    {
       
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_properties);
        
        _server=new SojournServer(HOST_URL);
        
        _pbLoading=(ProgressBar)findViewById(R.id.pbLoading);
        _btnRetry=(ImageButton)findViewById(R.id.btnRetry);
        _listProperties=(ListView)findViewById(R.id.listProperties);
        _listProperties.setOnItemClickListener(this);
        _txtNoResults=(TextView)findViewById(R.id.txtNoResults);
        
        Spinner s = (Spinner) findViewById(R.id.price_spinner);
        ArrayAdapter adapter = ArrayAdapter.createFromResource(
                this, R.array.price_op, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(
                                android.R.layout.simple_spinner_dropdown_item);
        s.setAdapter(adapter);
        _connectToServer();
    }
    
    @Override
    public void onStart()
    {
        
        super.onStart();
    }
    
    /** Callback for the Retry button */
    public void onRetryConnect(View button)
    {
        _connectToServer();
    }
    
    /** Retrieves a list of available properties asynchronously */
    private void _connectToServer()
    {   
        try
        {
            new GetPropertyList().execute(new URI("/properties"));
        }
        catch (URISyntaxException e)
        {
            Log.e(getClass().getSimpleName(),e.getMessage());
        }
    }
    
    /** Performs an HTTP GET request to the Sojourn Server and calls
     * _showProperties() on success. Calls that update UI elements are
     * wrapped inside Runnable objects to make sure that only the UI thread
     * performs UI updates (otherwise the activity crashes).
     * @author vpetrov
     *
     */
    private class GetPropertyList extends AsyncTask<URI, Void, JSONObject>
    {
        private Handler _handler=new Handler(Looper.getMainLooper());
        
        @Override
        protected JSONObject doInBackground(URI... params)
        {
            JSONObject result=null;
            
            //display the progress bar
            _uiShowProgressBar();
            
            try
            {
                //fetch a list of properties as JSON
                result=_server.getJSON(params[0]);
            }
            catch (SojournServerException e)
            {
                Log.e(this.getClass().getName(),e.getURI().toASCIIString()+": "+
                      e.getMessage());
                
                _uiShowErrorMessage();
            }
            
            return result;
        }
        
        protected void onPostExecute(JSONObject result)
        {
            //display the result,if available
            if (result!=null)
                _showProperties(result);
            else
                //otherwise, show the retry button
                _uiShowRetry();
        }
        
        /** hides other widgets and displays the ProgressBar */
        private void _uiShowProgressBar()
        {
            _handler.post(new Runnable()
            {
                public void run()
                {
                    _listProperties.setVisibility(View.GONE);
                    _pbLoading.setVisibility(View.VISIBLE);
                    _btnRetry.setVisibility(View.GONE);  
                    _txtNoResults.setVisibility(View.GONE);
                }
            });
        }
        
        /** Displays an error message when connection fails */
        private void _uiShowErrorMessage()
        {
            _handler.post(new Runnable()
            {
                public void run()
                {
                    Toast.makeText(getApplicationContext(),
                                   "We were unable to connect to the server."+
                                   "Please check your Internet connection.",
                                   Toast.LENGTH_LONG)
                         .show();
                }
            });
        }
        
        
        /** Calls _showRetry in a Runnable */
        private void _uiShowRetry()
        {
            _handler.post(new Runnable()
            {
                public void run()
                {
                    _showRetry();
                }
            });
            
        }
        

    }
    
    /** Hides other widgets and displays the Retry button */
    private void _showRetry()
    {
        _listProperties.setVisibility(View.GONE);
        _pbLoading.setVisibility(View.GONE);
        _txtNoResults.setVisibility(View.GONE);
        _btnRetry.setVisibility(View.VISIBLE);
    }
    
    /** Decodes the JSONObject into a list of Property objects and creates
     * a new Adapter for the ListView.
     * @param result
     */
    private void _showProperties(JSONObject result)
    {
        JSONArray items;
        int nitems;
               
        try
        {
            nitems=result.getInt("count");
            items=result.getJSONArray("result");
            
            if (nitems==0)
                 _showNoResults();
            else
            {
                _properties=new Property[nitems];
                
                //populate the Property list
                for (int i=0;i<nitems;++i)
                    _properties[i]=new Property(items.getJSONObject(i));
                
                
                //set a new adapter
                _listProperties.setAdapter(new PropertyAdapter(this,_server, 
                                                               _properties));

                _pbLoading.setVisibility(View.GONE);
                _btnRetry.setVisibility(View.GONE);
                _listProperties.setVisibility(View.VISIBLE);
            }
        }
        catch (JSONException e)
        {
            Log.e(getClass().getSimpleName(),"Failed to get results: "+
                  e.getMessage());
        }
    }

    @Override
    /** When an item is selected, start the ViewProperty activity */
    public void onItemClick(AdapterView<?>parent, View v, int position, long id)
    {
        Intent intent=new Intent(this,ViewProperty.class);
        
        intent.putExtra("HOST_URL",_server.getHost());
        intent.putExtra("property",_properties[position]);
        
        startActivity(intent);
    }
    
    @Override
    /** Prevent the activity from being restarted when orientation changes */
    public void onConfigurationChanged(Configuration config)
    {
        super.onConfigurationChanged(config);       
    }
    
    /** Callback for the Search button */
    public void btnSearchClicked(View button)
    {
        _doSearch();
    }
    
    /** If no results were found, show the txtNoResults widget and hide other
     * widgets.
     */
    private void _showNoResults()
    {
        _pbLoading.setVisibility(View.GONE);
        _btnRetry.setVisibility(View.GONE);
        _listProperties.setVisibility(View.GONE);
        _txtNoResults.setVisibility(View.VISIBLE);
    }
    
    /** Builds a search query, based on the values in the Search form.
     * If a value is empty, it is not included in the query params.
     * Also, it closes the SlidingDrawer and the keyboard.
     */
    private void _doSearch()
    {
        String url="/properties?";
        
        EditText editPrice=(EditText)findViewById(R.id.editPrice);
        EditText editBedrooms=(EditText)findViewById(R.id.editBedrooms);
        Spinner  spinnerPrice=(Spinner)findViewById(R.id.price_spinner);
        RatingBar searchRating=(RatingBar)findViewById(R.id.searchRating);
        SlidingDrawer drawer=(SlidingDrawer)findViewById(R.id.drawer);

        //hide the search form
        drawer.close();
        
        //close the soft keyboard
        InputMethodManager imm = (InputMethodManager)getSystemService(this.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editPrice.getWindowToken(), 0);
        imm.hideSoftInputFromWindow(editBedrooms.getWindowToken(), 0);
        
        //get form values
        String price=editPrice.getText().toString();
        String bedrooms=editBedrooms.getText().toString();
        String price_op=spinnerPrice.getSelectedItem().toString();
        String rating=String.valueOf(searchRating.getProgress());
        
        //filter by price?
        if (price.length()>0)
        {
            url+="&price="+URLEncoder.encode(price);
        
            if (price_op.length()>0)
                url+="&price_op="+URLEncoder.encode(price_op);
        }
        
        //filter by bedroom count?
        if (bedrooms.length()>0)
            url+="&bedrooms="+URLEncoder.encode(bedrooms);
        
        //filter by rating?
        if ((rating.length()>0) && (!rating.equals("0")))
            url+="&rating="+URLEncoder.encode(rating);
        
        try
        {
            //fetch the results asynchronously
            new GetPropertyList().execute(new URI(url));
        }
        catch (URISyntaxException e)
        {
            Log.e(getClass().getSimpleName(),"invalid URI: "+url);
        }        
    }
}