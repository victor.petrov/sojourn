/** Victor Petrov
* victor_petrov@harvard.edu
* 40750991 
*/
package net.cs76.projects.student40750991.server;

import java.net.URI;

/** SojournServerException is a container for exceptions that occur during the
* processing of an HTTP request to the Sojourn server
*/
public class SojournServerException extends Exception
{
    //for compatibility with Serializable
    private static final long serialVersionUID =1L;
    
    //the URI that caused an Exception
    private URI _uri;
    
    /** A server exception from a message string
    * @param msg The error message
    * @param u   The URL that caused the exception
    */
    public SojournServerException(String msg, URI u)
    {
        super(msg);
        _uri=u;
    }
    
    /** A server exception from another Throwable
    * @param u The URL that caused the Exception 
    * @param e The original Exception that was thrown
    */
    public SojournServerException(URI u, Throwable e)
    {
        super(e);
        _uri=u;
    }
    
    /** A server exception from a message string and another Throwable
    * @param msg The error message
    * @param u   The URL that caused the Exception
    * @param e   The original Exception that was thrown
    */
    public SojournServerException(String msg, URI u, Throwable e)
    {
        super(msg+": "+e.getMessage());
        _uri=u;
    }
    
    /** Returns the URI associated with this server exception
    * @return URI 
    */
    public URI getURI()
    {
        return _uri;
    }
}
