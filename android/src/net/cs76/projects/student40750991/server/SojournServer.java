/** Victor Petrov
* victor_petrov@harvard.edu
* 40750991 
*/
package net.cs76.projects.student40750991.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.*;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

/** The class SojournServer is a convenient wrapper around HttpGet and related
* classes. It performs HTTP GET requests to the Sojourn server and returns
* JSONObjects or Bitmaps. All downloaded images are cached.
* @see getJSON()
* @see getBitmap()
*/
public class SojournServer
{
    private String _host;
    private HashMap<String,Bitmap> _imageCache;
    
    /** SojournServer constructor. Sets the host address and instantiates the
    * image cache.
    * @param host Server URL (e.g. http://www.example.com/foo )
    */
    public SojournServer(String host)
    {
        _host=host;
        _imageCache=new HashMap<String,Bitmap>();
    }
    
    /** Returns the host address */
    public String getHost()
    {
        return _host;
    }
    
    /** Appends a relative URI to the host address and returns an absolute
     * URI. 
     * @param u A relative request URI
     * @return An absolute URI
     * @throws SojournServerException
     */
    private URI _getRequestURI(URI u) throws SojournServerException
    {
        URI url=null;
        
        try
        {
            //Append the relative URI to the host address
            url=new URI(_host+u);
        }
        catch (URISyntaxException e)
        {
            throw new SojournServerException("Failed to parse request URI: "+
                                             _host.toString()+u.toString(),
                                             u,e);
        }
        
        return url;
    }
    
    /** Performs an HTTP GET request to the server and decodes the response into
     * JSONObjects.
     * @param u The resource URI
     * @return JSONObject
     * @throws SojournServerException
     */
    public JSONObject getJSON(URI u) throws SojournServerException
    {
        HttpGet request=new HttpGet(_getRequestURI(u));
        HttpResponse response;
        String content;
        JSONObject result;
        
        //perform the HTTP request
        response=_send(request);
        
        try
        {
            //read response data
            content=_getResponseBody(response.getEntity().getContent());
        }
        catch (IOException e)
        {
            throw new SojournServerException("Failed to read response data",
                                             request.getURI(),e);
        }
        
        if (content==null)
            throw new SojournServerException("Empty response from server",
                                             request.getURI());
        
        try
        {
            //decode the JSON string into a JSONObject
            result=new JSONObject(content);
        }
        catch (JSONException e)
        {
            throw new SojournServerException("Failed to read JSON response",
                                             request.getURI(),e);
        }

        return result;
    }
    
    /** Downloads an image from the server. This URI is usually absolute,
     * because the Sojourn server returns full URIs for the location of the 
     * images (which may be located on other CDN servers)
     * @param u Image URI
     * @return A Bitmap
     * @throws SojournServerException
     */
    public Bitmap getBitmap(URI u) throws SojournServerException
    {
        Bitmap result=null;        
        HttpGet request=new HttpGet(u);
        HttpResponse response;
        
        //attempt to locate the image in the cache
        if (_imageCache.containsKey(u.toString()))
            return _imageCache.get(u.toString());
        
        //on cache miss, request the image
        response=_send(request);
        
        try
        {
            //decode the image data
            result=BitmapFactory.decodeStream(response.getEntity()
                                              .getContent());
        }
        catch (IOException e)
        {
            throw new SojournServerException("Failed to decode image data",
                                             request.getURI(),e);
        }
        
        //store the Bitmap object in the image cache
        if (result!=null)
            _imageCache.put(u.toString(),result);
        
        return result;
    }
    
    /** Performs an HTTP request to the Sojourn server and checks the response
     * code. If the code is not 200, a SojournServerException is thrown. 
     * @param request The HTTP request object (usually, HttpGet)
     * @return The HTTP response object
     * @throws SojournServerException
     */
    private HttpResponse _send(HttpRequestBase request) 
            throws SojournServerException
    {
        HttpResponse result;
        DefaultHttpClient  client;
        StatusLine  status;

        //create a new HTTP client with default settings
        client=new DefaultHttpClient();

        try
        {
            //send the request to the server
            result=client.execute(request);
        }
        catch (IOException e)
        {
            throw new SojournServerException("Server request failed",
                                             request.getURI(),e);
        }
        
        //store the status line
        status=result.getStatusLine();
        
        //check the return code
        if (status.getStatusCode()!=200)
            throw new SojournServerException("Server request failed: "+
                                             status.getStatusCode()+
                                             status.getReasonPhrase(),
                                             request.getURI());
        
        //return the response object
        return result;
    }
    
    /** Reads data from an InputStream and returns a String object.
     * @param input The input stream
     * @return The content string
     * @throws IOException
     */
    private String _getResponseBody(InputStream input) throws IOException
    {
        String result=null;
        BufferedReader in;
        String line;
        
        //prepare to read the content
        in = new BufferedReader(new InputStreamReader(input));
        
        //the data will be placed in a StringBuffer
        StringBuffer sb=new StringBuffer("");
        
        //read the content line by line and append it to the StringBuffer
        while ((line=in.readLine())!=null)
            sb.append(line);
        
        //convert the StringBuffer into a String
        result=sb.toString();

        //return the data as a String
        return result;
    }
    

}
