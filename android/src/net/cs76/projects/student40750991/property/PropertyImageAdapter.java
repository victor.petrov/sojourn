/** Victor Petrov
* victor_petrov@harvard.edu
* 40750991 
*/
package net.cs76.projects.student40750991.property;

import net.cs76.projects.student40750991.R;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.ProgressBar;

/** A simple image adapter for the image gallery.
 * The images are read from an array, which is populated asynchronously by an
 * external entity. While the images are being downloaded, a ProgressBar is
 * shown instead of the normal ImageView. When the image becomes available,
 * it replaces the ProgressBar.
 */
public class PropertyImageAdapter extends BaseAdapter
{
    Context  _context;
    Bitmap   _images[];
    int _width;
    int _height;
    int _imageBackground;
    
    /** Constructs a new PropertyImageAdapter. Takes a reference to a Bitmap
     * array, which may contain null values (if so, a ProgressBar is shown).
     * @param c      The Context object
     * @param images An array of Bitmap objects and/or null's
     */
    public PropertyImageAdapter(Context c, Bitmap images[])
    {
        TypedArray attr;
        _context=c;
        _images=images;
        _width=200;
        _height=150;
        
        //These attributes are used to style each image in the Gallery.
        attr=_context.obtainStyledAttributes(R.styleable.PropertyImageGallery);
        _imageBackground=attr.getResourceId(
            R.styleable.PropertyImageGallery_android_galleryItemBackground, 0);
        attr.recycle();
    }

    @Override
    /** Returns the total number of images */ 
    public int getCount()
    {
        return _images.length;
    }

    @Override
    /** Returns the image at the specified position */
    public Object getItem(int position)
    {
        return _images[position];
    }

    @Override
    /* Returns the given position as the item ID */
    public long getItemId(int position)
    {
        return (long)position;
    }

    @Override
    /** Creates a View suitable for display in an Image Gallery. If there is no
     * image available at the time of the call, this function returns a new
     * ProgressBar. If the image Bitmap exists, it returns an ImageView instead.
     * @note Returning the same ProgressBar object does not result in multiple
     *       ProgressBar's being shown, hence the call to _newProgressBar().
     */
    public View getView(int position, View convertView, ViewGroup parent)
    {
        //check if the image exists
        if (_images[position]==null)
        {
            //if not, create a new ProgressBar
            convertView=_newProgressBar();
        }
        
        //if the image exists, attempt to reuse an existing View object
        else if (convertView!=null)
        {
            ((ImageView)convertView).setImageBitmap(_images[position]);
        }
        
        //if the image exists, but there is no View object available, create one
        else
        {
            ImageView imageView=new ImageView(_context);
            imageView.setLayoutParams(new Gallery.LayoutParams(_width,_height));
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            imageView.setImageBitmap(_images[position]);
            imageView.setBackgroundResource(_imageBackground);
            
            convertView=imageView;
        }
        
        //return either the ProgressBar or the ImageView
        return convertView;
    }
    
    /** Creates a new ProgressBar, which spins forever.
     * @return A ProgressBar object
     */
    private ProgressBar _newProgressBar()
    {
        ProgressBar progressBar;
        progressBar=new ProgressBar(_context,null,
                                    android.R.attr.progressBarStyleLarge);
        progressBar.setLayoutParams(new Gallery.LayoutParams(
                                        LayoutParams.WRAP_CONTENT,
                                        LayoutParams.WRAP_CONTENT));
        progressBar.setIndeterminate(true);
        progressBar.setVisibility(View.VISIBLE);
        
        return progressBar;
    }

}
