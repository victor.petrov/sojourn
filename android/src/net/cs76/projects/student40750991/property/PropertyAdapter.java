/** Victor Petrov
* victor_petrov@harvard.edu
* 40750991 
*/
package net.cs76.projects.student40750991.property;

import java.net.URI;
import java.util.HashMap;

import net.cs76.projects.student40750991.R;
import net.cs76.projects.student40750991.server.SojournServer;
import net.cs76.projects.student40750991.server.SojournServerException;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/** An adapter which is able to display the Property thumbnail,title and address
 * The thumbnail is downloaded asynchronously, to avoid blocking the UI thread
 * and is stored in a HashMap, based on the Property ID.
 * This class inflates the layout of each item from res/layouts/property.xml
 * To avoid calling findViewById() each time an item should be redrawn,
 * PropertyAdapter uses the ViewHolder pattern to store references to View
 * children using getTag() and setTag(). 
 * @see GetPropertyThumbnail
 * @see ViewHolder
 */
public class PropertyAdapter extends BaseAdapter
{
    private Property _properties[];
    private LayoutInflater _inflater;
    private SojournServer _server;
    private HashMap<Integer, Bitmap> _thumbnails;
    
    public PropertyAdapter(Context context, SojournServer server, 
                           Property properties[])
    {
        _server=server;
        _properties=properties;
        _inflater=LayoutInflater.from(context);
        _thumbnails=new HashMap<Integer, Bitmap>();
    }

    @Override
    public int getCount()
    {
        return _properties.length;
    }

    @Override
    public Object getItem(int position)
    {
        return _properties[position];
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    @Override
    /** Creates a new View, or reuses an existing View, based on the layout
     * from res/layouts/property.xml. Uses the ViewHolder pattern to store 
     * references to child widgets.
     */
    public View getView(int position, View convertView, ViewGroup parent)
    {
        ViewHolder holder;
        Bitmap thumbnail;
        Property p;
        
        //get the Property object
        p=_properties[position];
        
        //get the Thumbnail image
        thumbnail=_thumbnails.get(p.getId());
        
        if (convertView==null)
        {
            //inflate the property layout
            convertView=_inflater.inflate(R.layout.property,null);
            
            //store widget references in a ViewHolder
            holder=new ViewHolder();
            holder.thumbnail=(ImageView)convertView.findViewById(R.id.imgThumbnail);
            holder.title=(TextView)convertView.findViewById(R.id.txtTitle);
            holder.address=(TextView)convertView.findViewById(R.id.txtAddress);
            
            //save the ViewHolder object as the tag
            convertView.setTag(holder);
        }
        else
        {
            //get the ViewHolder object
            holder=(ViewHolder)convertView.getTag();
        }
        
        //If the thumbnail is available, display the bitmap (already resized)
        if (thumbnail!=null)
            holder.thumbnail.setImageBitmap(thumbnail);
        else
            //otherwise, download the thumbnail asynchronously
            (new GetPropertyThumbnail()).execute(p);
            
        holder.title.setText(p.getTitle());
        holder.address.setText(p.getFullAddress());
        
        return convertView;
    }
    
    /** Stores the new Bitmap and tells this Adapter the dataset has changed */
    private void _updateBitmap(Property p, Bitmap b)
    {
        _thumbnails.put(p.getId(),b);
        this.notifyDataSetChanged();
    }

    /** Stores references to child widgets */
    static class ViewHolder
    {            
        ImageView thumbnail;
        TextView title;            
        TextView address;        
    } 

    /** Retrieves the thumbnail image asynchronously, using an AsyncTask */
    private class GetPropertyThumbnail extends AsyncTask<Property, Void, Bitmap>
    {
        private Property _property;
        
        @Override
        protected Bitmap doInBackground(Property... params)
        {
            Bitmap result=null;
            _property=params[0];
            
            try
            {
                //get the Thumbnail URL
                URI url=_property.getThumbnail();
                
                //download the image, if the URL is valid
                if (url.getHost()!=null)
                    result=_server.getBitmap(url);
            }
            catch (SojournServerException e)
            {
                Log.e(this.getClass().getName(),e.getURI().toASCIIString()+": "+
                      e.getMessage());   
            }
            
            return result;
        }
        
        protected void onPostExecute(Bitmap result)
        {
            //update the thumbnail array
            if (result!=null)
                _updateBitmap(_property,result);
        }
    }
}
