/** Victor Petrov
* victor_petrov@harvard.edu
* 40750991 
*/
package net.cs76.projects.student40750991.property;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Point;

/** A simple Google Maps Overlay, which displays a Pin image at the specified
* point on the Map.
*/
public class Pin extends Overlay
{
    private Bitmap _pin;
    private GeoPoint _point;
    
    /** Creates a new Pin overlay using an existing Bitmap and a map point */ 
    public Pin(GeoPoint point, Bitmap pin)
    {
        _point=point;
        _pin=pin;
    }
    
    @Override
    /** Draws the pin Bitmap */
    public boolean draw(Canvas canvas, MapView mapView, boolean shadow, 
                        long when)
    {
        super.draw(canvas,mapView,shadow);

        //translate the GeoPoint to screen pixels
        Point screenPts=new Point();
        mapView.getProjection().toPixels(_point,screenPts);

        //draw the pin
        canvas.drawBitmap(_pin,screenPts.x,screenPts.y-_pin.getHeight(),null);
        return true;
    }
}
