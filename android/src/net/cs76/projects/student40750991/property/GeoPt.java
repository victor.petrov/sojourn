/** Victor Petrov
* victor_petrov@harvard.edu
* 40750991 
*/
package net.cs76.projects.student40750991.property;

/** A class which encapsulates the SojournServer's idea of a GeoPoint.
 * The 2 helper methods, getLat() and getLon(), return values suitable for
 * instantiating a Google Map GeoPoint object.
 */
public class GeoPt
{
    private double _latitude;
    private double _longitude;
    
    public GeoPt()
    {
        _latitude=0.0f;
        _longitude=0.0f;
    }
    
    public GeoPt(double latitude)
    {
        _latitude=latitude;
    }
    
    public GeoPt(double latitude, double longitude)
    {
        _latitude=latitude;
        _longitude=longitude;
    }
    
    public double getLatitude()
    {
        return _latitude;
    }
    
    /** returns an int value, as required by GMaps GeoPoint */
    public int getLat()
    {
        return (int)(_latitude*1e6);
    }
       
    public double getLongitude()
    {
        return _longitude;
    }
    
    /** returns an int value, as required by GMaps GeoPoint */
    public int getLon()
    {
        return (int)(_longitude*1e6);
    }
    
    public void setLatitude(double latitude)
    {
        _latitude=latitude;
    }
    
    public void setLongitude(double longitude)
    {
        _longitude=longitude;
    }
}
