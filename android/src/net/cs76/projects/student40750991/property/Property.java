/** Victor Petrov
* victor_petrov@harvard.edu
* 40750991 
*/
package net.cs76.projects.student40750991.property;

import java.net.URI;
import java.net.URISyntaxException;

import org.json.*;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

/** A container encapsulting all known elements of a Property (real estate).
 * This object implements Parcelable in order to be passed as an Intent extra.
 * In addition, a Property object can be instantiated from a JSONObject
 */
public class Property implements Parcelable
{
    private int _id;
    private String _title;
    private String _description;
    private boolean _availability;
    private double _price;
    private int _nbedrooms;
    private GeoPt _coord;
    private String _address;
    private String _postalcode;
    private String _city;
    private String _state;
    private String _country;
    private int _rating;
    private URI _thumbnail;
    private URI _images[];
    private int _owner;
    
    public Property() {}
    
    /** Creates a new Property object from a JSONObject */
    public Property(JSONObject json)
    {
        JSONArray img;
        JSONObject coords;
        
        try
        {
            _id=json.getInt("_id");
            _title=json.getString("title");
            _description=json.getString("description");
            _availability=json.getBoolean("availability");
            _price=json.getDouble("price");
            _nbedrooms=json.getInt("nbedrooms");
            coords=json.getJSONObject("coord");
            _coord=new GeoPt(coords.getDouble("latitude"),
                             coords.getDouble("longitude"));
            _address=json.getString("address");
            _postalcode=json.getString("postalcode");
            _city=json.getString("city");
            _state=json.getString("state");
            _country=json.getString("country");
            _rating=json.getInt("rating");
            String thumbnailURL=json.getString("thumbnail");
            
            try
            {
                _thumbnail=new URI(thumbnailURL);
            }
            catch (URISyntaxException e)
            {
                Log.e(getClass().getSimpleName(),"Invalid URI: "+thumbnailURL);
            }
            
            img=json.getJSONArray("images");
            
            int nimg=img.length();
            _images=new URI[nimg];
            String uri;
            
            //Create URI objects for each image address found
            for (int i=0;i<nimg;++i)
            {
                uri=img.getString(i);
                        
                try
                {
                    _images[i]=new URI(uri);
                }
                catch (URISyntaxException e)
                {
                    Log.e(getClass().getSimpleName(),"Invalid URI: "+uri);
                }
            }
        }
        catch (JSONException e)
        {
            Log.e(getClass().getSimpleName(),e.getMessage());
        }
    }
    
    /** Creates a new Property object from a Parcel object. */
    public Property(Parcel p)
    {
        _id=p.readInt();
        _title=p.readString();
        _description=p.readString();
        _availability=((p.readInt()==1)?true:false);
        _price=p.readDouble();
        _nbedrooms=p.readInt();
        _coord=new GeoPt(p.readDouble(),p.readDouble());
        _address=p.readString();
        _postalcode=p.readString();
        _city=p.readString();
        _state=p.readString();
        _country=p.readString();
        _rating=p.readInt();
        
        try
        {
            String thumbnail=p.readString();
            _thumbnail=new URI(thumbnail);
            
            int nimages=p.readInt();
            
            if (nimages>0)
            {
                _images=new URI[nimages];
                
                String images[]=new String[nimages];
                p.readStringArray(images);
               
                for (int i=0;i<nimages;++i)
                    _images[i]=new URI(images[i]);
            }
        }
        catch (URISyntaxException e)
        {
            Log.e(getClass().getSimpleName(),"Failed to read URL from Parcel: "+
                  e.getMessage());
        }
    }
    
    public int getId()
    {
        return _id;
    }
    
    public String getTitle()
    {
        return _title;
    }
    
    public String getDescription()
    {
        return _description;
    }
    
    public boolean getAvailability()
    {
        return _availability;
    }
    
    public double getPrice()
    {
        return _price;
    }
    
    public int getBedroomCount()
    {
        return _nbedrooms;
    }
    
    public GeoPt getGeoCoords()
    {
        return _coord;
    }
    
    public String getAddress()
    {
        return _address;
    }
    
    /** Returns a concatenation of address,city,state,zip and country */
    public String getFullAddress()
    {
        String result=_address;
        
        if (_city.length()>0)
            result+=", "+_city;
        
        if (_state.length()>0)
            result+=", "+_state;
        
        if (_postalcode.length()>0)
            result+=", "+_postalcode;
        
        if (_country.length()>0)
            result+=", "+_country;
        
        return result;
    }
    
    public String getPostalCode()
    {
        return _postalcode;
    }
    
    public String getCity()
    {
        return _city;
    }
    
    public String getState()
    {
        return _state;
    }
    
    public String getCountry()
    {
        return _country;
    }
    
    public int getRating()
    {
        return _rating;
    }
    
    public URI getThumbnail()
    {
        return _thumbnail;
    }
    
    public URI[] getImages()
    {
        return _images;
    }
    
    public int getOwnerId()
    {
        return _owner;
    }
    
    public void setId(int id)
    {
        _id=id;
    }
    
    public void setTitle(String title)
    {
        _title=title;
    }
    
    public void setDescription(String description)
    {
        _description=description;
    }
    
    public void setAvailability(boolean availability)
    {
        _availability=availability;
    }
    
    public void setPrice(double price)
    {
        _price=price;
    }
    
    public void setBedroomCount(int nbedrooms)
    {
        _nbedrooms=nbedrooms;
    }
    
    public void setGeoCoords(double latitude, double longitude)
    {
        _coord=new GeoPt(latitude,longitude);
    }
    
    public void setGeoCoords(GeoPt coord)
    {
        _coord=coord;
    }
    
    public void setAddress(String address)
    {
        _address=address;
    }
    
    public void setPostalCode(String postalcode)
    {
        _postalcode=postalcode;
    }
    
    public void setCity(String city)
    {
        _city=city;
    }
    
    public void setState(String state)
    {
        _state=state;
    }
    
    public void setCountry(String country)
    {
        _country=country;
    }
    
    public void setRating(int rating)
    {
        _rating=rating;
    }
    
    public void setThumbnail(URI thumbnail)
    {
        _thumbnail=thumbnail;
    }
    
    public void setImages(URI images[])
    {
        _images=images;
    }
    
    public void setOwnerId(int owner)
    {
        _owner=owner;
    }

    @Override
    public int describeContents()
    {
        return 0;
    }
    
    @Override
    /** Serializes this Property object into a Parcel object */
    public void writeToParcel(Parcel dest, int flags)
    {      
        dest.writeInt(_id);
        dest.writeString(_title);
        dest.writeString(_description);
        dest.writeInt(_availability?1:0);
        dest.writeDouble(_price);
        dest.writeInt(_nbedrooms);
        dest.writeDouble(_coord.getLatitude());
        dest.writeDouble(_coord.getLongitude());
        dest.writeString(_address);
        dest.writeString(_postalcode);
        dest.writeString(_city);
        dest.writeString(_state);
        dest.writeString(_country);
        dest.writeInt(_rating);
        dest.writeString(_thumbnail.toString());
        
        int nimages=_images.length;
        
        //store the number of images
        dest.writeInt(nimages);

        //store all image URIs
        if (nimages>0)
        {
            String imageURLs[]=new String[nimages];
            
            for (int i=0;i<nimages;++i)
                imageURLs[i]=_images[i].toString();
            
            dest.writeStringArray(imageURLs);
        }
    }
    
    /** Parcelable requires a static CREATOR member */
    public static final Parcelable.Creator<Property> CREATOR = 
     new Parcelable.Creator<Property>()
     {
         public Property createFromParcel(Parcel in)
         {
             return new Property(in);
         }
    
         public Property[] newArray(int size)
         {
             return new Property[size];
         }
      };

}
