/** Victor Petrov
* victor_petrov@harvard.edu
* 40750991 
*/
package net.cs76.projects.student40750991;

import java.net.URI;

import net.cs76.projects.student40750991.property.Property;
import net.cs76.projects.student40750991.property.PropertyImageAdapter;
import net.cs76.projects.student40750991.server.SojournServer;
import net.cs76.projects.student40750991.server.SojournServerException;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TabHost;
import android.widget.TextView;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapController;
import com.google.android.maps.MapView;

import net.cs76.projects.student40750991.property.Pin;

/** An activity which displays 3 tabs: Details, Images and Map.
 * A Property object is expected to be passed as an extra Parcelable object.
 * The SojournServer host URL should also be passed as an extra String property.
 * The Details tab is populated in the constructor, by updating the values of
 * each widget.
 * The Images tab uses an ImageGallery and an ImageView. The Bitmaps are
 * downloaded asynchronously, using the URLs from Property.getImages().
 * The Map tab uses a Google Map view to zoom in on the geographical location
 * of the Property and to place a Pin on the map at that location.
 *
 */
//API CHANGE (APICHANGE): MapActivity requires Google target build, 
//though the API level is still technically 7
public class ViewProperty extends MapActivity implements OnItemClickListener
{    
    SojournServer   _server;
    Property        _property;
    MapView         _map;
    GeoPoint        _location;
    Bitmap          _images[];
    PropertyImageAdapter _imgAdapter;
    ImageView       _imgViewer;
    
    
    @Override
    /** Updates the values of all widgets defined in layout/view_property.xml.
     * Creates 3 tabs: Details, Images and Map. Initiates the asynchronous
     * image download.
     */
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.view_property);
        
        //get extra params
        Bundle extras=this.getIntent().getExtras();
        String host=extras.getString("HOST_URL");
        
        _server=new SojournServer(host);
        _property=extras.getParcelable("property");

        //update widget values
        TextView title=(TextView)findViewById(R.id.txtTitle);
        TextView description=(TextView)findViewById(R.id.txtDescription);
        TextView address=(TextView)findViewById(R.id.txtAddress);
        TextView bedrooms=(TextView)findViewById(R.id.txtBedrooms);
        TextView price=(TextView)findViewById(R.id.txtPrice);
        
        //rating
        RatingBar ratingBar=(RatingBar)findViewById(R.id.ratingBar);
        ratingBar.setRating((float)_property.getRating());
        
        //map
        _map = (MapView) findViewById(R.id.map);
        _map.setBuiltInZoomControls(true);
        
        _location=new GeoPoint(_property.getGeoCoords().getLat(),
                               _property.getGeoCoords().getLon());

        //other property details
        title.setText(_property.getTitle());
        description.setText(_property.getDescription());
        address.setText(_property.getFullAddress());
        bedrooms.setText(String.valueOf(_property.getBedroomCount()));
        price.setText("$"+String.valueOf(_property.getPrice()));
        
        //setup the tab host
        Resources res=getApplication().getResources();
        TabHost tabs = (TabHost)findViewById(R.id.tabHost);
        tabs.setup();

        //create a new tab for property Details
        TabHost.TabSpec tabDetails=tabs.newTabSpec("Details");
        tabDetails.setContent(R.id.details);
        tabDetails.setIndicator("Details",res.getDrawable(R.drawable.ic_tab_info));
        tabs.addTab(tabDetails);
        
        //create a new tab for the Pictures
        TabHost.TabSpec tabImages=tabs.newTabSpec("Images");
        tabImages.setContent(R.id.imgGalleryLayout);
        tabImages.setIndicator("Images",res.getDrawable(R.drawable.ic_tab_picture));
        tabs.addTab(tabImages);
        
        //create a new tab for the Map
        TabHost.TabSpec tabMaps=tabs.newTabSpec("Map");
        tabMaps.setContent(R.id.map);
        tabMaps.setIndicator("Map",res.getDrawable(R.drawable.ic_tab_map));
        tabs.addTab(tabMaps);
        
        tabs.setCurrentTab(0);
        
        //create the Pin Overlay
        Bitmap imgPin=BitmapFactory.decodeResource(getResources(),R.drawable.ic_pin);
        Pin pin=new Pin(_location,imgPin);
        
        _map.getOverlays().add(pin);
        
        //set up the Gallery and download images, if there are image links
        if (_property.getImages()!=null)
        {
            _images=new Bitmap[_property.getImages().length];     
            _imgAdapter=new PropertyImageAdapter(this,_images);
            Gallery imgGallery=(Gallery)findViewById(R.id.imgGallery);
            imgGallery.setAdapter(_imgAdapter);
            imgGallery.setOnItemClickListener(this);
            
            _imgViewer=(ImageView)findViewById(R.id.imgViewer);
            
            _downloadImages();
        }
    }
    
    /** Centers the map. */
    private void mapGoTo(GeoPoint location)
    {
        MapController mc=_map.getController();
        
        mc.setCenter(location);
        mc.setZoom(18);
        _map.invalidate();
    }
    
    @Override
    /** Update the Map view */
    public void onResume()
    {
        super.onResume();
        
        mapGoTo(_location);
    }

    @Override
    protected boolean isRouteDisplayed()
    {
        return false;
    }
    
    @Override
    /** Prevent the activity from being restarted when orientation changes */
    public void onConfigurationChanged(Configuration config)
    {
        super.onConfigurationChanged(config);       
    }
    
    /** Callback for the button on the title bar */
    public void btnBackClicked(View view)
    {
        finish();
    }
    
    /** Starts the asynchronous download of all images */
    private void _downloadImages()
    {
        int nimages=_property.getImages().length;
        
        for (int i=0;i<nimages;++i)
            (new GetPropertyImage()).execute(i);
    }
    
    /** Downloads an images asynchronously, to avoid blocking the UI thread. */
    private class GetPropertyImage extends AsyncTask<Integer, Void, Bitmap>
    {
        int _id;
        
        @Override
        protected Bitmap doInBackground(Integer... params)
        {
            Bitmap result=null;
            _id=params[0];
            URI urls[]=_property.getImages();
            
            try
            {
                //fetch the image
                result=_server.getBitmap(urls[_id]);
            }
            catch (SojournServerException e)
            {
                Log.e(this.getClass().getName(),e.getURI().toASCIIString()+": "+
                      e.getMessage());   
            }
            
            return result;
        }
        
        protected void onPostExecute(Bitmap result)
        {
            //update the image array and display the first image, if available
            if (result!=null)
            {
                _images[_id]=result;
                _imgAdapter.notifyDataSetChanged();
                if ((_imgViewer.getDrawable()==null) && (_images[0]!=null))
                    _imgViewer.setImageBitmap(_images[0]);
            }
        }
    }

    @Override
    /** when an image is clicked in the Gallery, display the large version */ 
    public void onItemClick(AdapterView<?> parent,View view,int position,
                            long id)
    {
        if (_images[position]!=null)
            _imgViewer.setImageBitmap(_images[position]);
    }

}