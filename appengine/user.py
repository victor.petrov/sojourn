#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from google.appengine.ext import webapp
from google.appengine.ext.webapp import util
from google.appengine.ext import db
from google.appengine.ext.webapp import template

from time import sleep

from models import User

from django.utils import simplejson as json
from hashlib import sha1
import os

import random

def UserToDict(u):
    uid=u.key().id();
    
    return {
              "_id":uid,
              "_url":'/user/%d' % uid,
              "firstName":u.firstName,
              "lastName":u.lastName,
              "email":u.email
           }
def UserIdToDict(uid):
    return {
            "_id":uid,
            "_url":'/user/%d' % uid
           }

#===============================================================================
# API HANDLERS
#===============================================================================

class UserListHandler(webapp.RequestHandler):
    def get(self):
        users=[]
        
        #get all users
        q=db.GqlQuery("SELECT * FROM User")
        result=q.fetch(100)
        
        self.response.headers.add_header('Content-Type','application/json')
        
        #loop over all results
        for u in result:
            users.append(UserToDict(u));
            
        sleep(3) #DELETE ME!
        
        #print all users as JSON
        self.response.out.write(json.dumps( {
                                             "success":1,
                                             "count":len(users),
                                             "result":users
                                            }))

class UserHandler(webapp.RequestHandler):
    
    def get(self,pID):
        u=User.get_by_id(int(pID))
        found=int(u!=None)
            
        self.response.out.write(json.dumps( {
                                             "success":1,
                                             "count":found,
                                             "result":[UserToDict(u)]
                                            }))

    def put(self):
        new_user=User(firstName="Victor"+str(random.randint(1,1000)),
                     lastName="Petrov",
                     email="victor.petrov@gmail.com",
                     password="test")
        key=new_user.put()
        
        self.response.out.write(json.dumps({
                                            "success":1,
                                            "count":1,
                                            "result":[UserIdToDict(key.id())]
                                           }))
        

#===============================================================================
# UI HANDLERS
#===============================================================================

#Displays the Add user form
class UserFormHandler(webapp.RequestHandler):
    def get(self):
        tpl_path = os.path.join(os.path.dirname(__file__), 'templates/user.html')
        tpl_data = []
        
        self.response.out.write(template.render(tpl_path, tpl_data))
        
    def post(self):
        firstName=self.request.get('firstName')
        lastName=self.request.get('lastName')
        email=self.request.get('email')
        password=self.request.get('password')
        
        user=User(firstName=firstName,
                  lastName=lastName,
                  email=email,
                  password=sha1(password).hexdigest())
        
        key=user.put()
        
        self.response.out.write(json.dumps({
                                            "success":1,
                                            "count":1,
                                            "result":[UserIdToDict(key.id())]
                                           }))


#===============================================================================
# MAIN
#===============================================================================
def main():
    application = webapp.WSGIApplication([('/users', UserListHandler),
                                          ('/user',UserFormHandler),
                                          ('/user/(\d+)',UserHandler)
                                         ],
                                         debug=True)
    util.run_wsgi_app(application)


if __name__ == '__main__':
    main()
