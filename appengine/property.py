#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
from google.appengine.ext import webapp
from google.appengine.ext.webapp import util, blobstore_handlers
from google.appengine.ext import db
from google.appengine.ext.webapp import template
from google.appengine.ext.db import GeoPt, PostalAddress
from google.appengine.api import images


from models import Property

from django.utils import simplejson as json
import os
from google.appengine.ext.blobstore import blobstore

def PropertyToDict(p):
    pid=p.key().id();
    
    thumbnail=""
    
    if p.thumbnail!=None:
        thumbnail=images.get_serving_url(str(p.thumbnail.key()))
    
    result= {
                "_id":pid,
                "title":p.title,
                "description":p.description,
                "availability":p.availability,
                "price":p.price,
                "nbedrooms":p.nbedrooms,
                "coord":{'latitude':p.coord.lat,
                         'longitude':p.coord.lon
                         },
                "address":p.address,
                "postalcode":p.postalcode,
                "city":p.city,
                "state":p.state,
                "country":p.country,
                "images":[],
                "thumbnail":thumbnail,
                "owner":{
                         "_id":p.owner,
                         "_url":'/user/'+ str(p.owner)
                        },
                "rating":p.rating
           }
    
    for img in p.images:
        url=images.get_serving_url(str(img))
        result['images'].append(url);

    return result;
    
def PropertyIdToDict(pid):
    return {
            "_id":pid,
            "_url":'/property/%d' % pid
           }

#===============================================================================
# API HANDLERS
#===============================================================================

class PropertyListHandler(webapp.RequestHandler):
    def get(self):
        properties=[]
        
        price=self.request.get('price')
        price_op=self.request.get('price_op')
        bedrooms=self.request.get('bedrooms')
        rating=self.request.get('rating')
        
        #Begin query
        q=db.Query(Property)
        
        #filter by price?
        if len(price):
            if price_op==">":
                op=">";
            elif price_op=="<":
                op="<";
            else:
                op="=";
            
            q.filter('price '+op, float(price))
            
        #filter by bedrooms?
        if len(bedrooms):
            q.filter('nbedrooms =',int(bedrooms))
            
        #filter by rating?
        if len(rating):
            q.filter('rating =',int(rating))

        result=q.fetch(100)
        
        self.response.headers.add_header('Content-Type','application/json')
        
        #loop over all results
        for p in result:
            properties.append(PropertyToDict(p));
            
        #print all properties as JSON
        self.response.out.write(json.dumps( {
                                             "success":1,
                                             "count":len(properties),
                                             "result":properties
                                            }))

class PropertyHandler(webapp.RequestHandler):
    
    def get(self,pID):
        p=Property.get_by_id(int(pID))
        found=int(p!=None)
            
        self.response.out.write(json.dumps( {
                                             "success":1,
                                             "count":found,
                                             "result":[PropertyToDict(p)]
                                            }))

#===============================================================================
# UI HANDLERS
#===============================================================================

#Displays the Upload form
class PropertyUploadFormHandler(webapp.RequestHandler):
    def get(self,pID):
        p=Property.get_by_id(int(pID))
        
        tpl_path=os.path.join(os.path.dirname(__file__),'templates/upload.html')
        tpl_data= {
                   'title':p.title,
                   'upload_url':blobstore.create_upload_url('/property/'+pID+'/upload/success')
                  }
        self.response.out.write(template.render(tpl_path, tpl_data))

class PropertyUploadSuccessHandler(blobstore_handlers.BlobstoreUploadHandler):
    def post(self,pID):
        p=Property.get_by_id(int(pID))
        
        thumb_upload=self.get_uploads('thumbnail')
        
        if len(thumb_upload)>0:
            thumb_blob=thumb_upload[0];
            p.thumbnail=thumb_blob

        images_uploaded=self.get_uploads('images')
        
        p.images=[];
        
        for img_blob in images_uploaded:
            p.images.append(img_blob.key())
        
        p.put();
        
        self.response.out.write("OK")

#Displays the Add Property form
class PropertyFormHandler(webapp.RequestHandler):
    def get(self):
        tpl_path = os.path.join(os.path.dirname(__file__), 'templates/property.html')
        tpl_data = []
        self.response.out.write(template.render(tpl_path, tpl_data))
        
    def post(self):
        title=self.request.get('title')
        description=self.request.get('description')
        availability=self.request.get('availability')
        if (availability is None) or (len(availability)==0):
            availability=False
        else:
            availability=bool(availability)
            
        price=float(self.request.get('price'))
        nbedrooms=int(self.request.get('nbedrooms'))
        
        coord=GeoPt(lat=self.request.get('latitude'),
                    lon=self.request.get('longitude'))
        address=PostalAddress(self.request.get('address'))
        postalcode=self.request.get('postalcode')
        city=self.request.get('city')
        state=self.request.get('state')
        country=self.request.get('country')
        rating=int(self.request.get('rating'))
        owner=int(self.request.get('owner'))
        
        p=Property(title=title,
                          description=description,
                          availability=availability,
                          price=price,
                          nbedrooms=nbedrooms,
                          coord=coord,
                          address=address,
                          postalcode=postalcode,
                          city=city,
                          state=state,
                          country=country,
                          rating=rating,
                          owner=owner)
        
        key=p.put()
        
        self.response.out.write(json.dumps({
                                            "success":1,
                                            "count":1,
                                            "result":[PropertyIdToDict(key.id())]
                                           }))


#===============================================================================
# MAIN
#===============================================================================
def main():
    application = webapp.WSGIApplication([('/properties', PropertyListHandler),
                                          ('/property',PropertyFormHandler),
                                          ('/property/(\d+)',PropertyHandler),
                                          ('/property/(\d+)/upload',PropertyUploadFormHandler),
                                          ('/property/(\d+)/upload/success',PropertyUploadSuccessHandler)
                                         ],
                                         debug=True)
    util.run_wsgi_app(application)


if __name__ == '__main__':
    main()
