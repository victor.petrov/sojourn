__author__ = 'vpetrov'

from google.appengine.ext import db
from google.appengine.ext.blobstore import blobstore


class User(db.Model):
    firstName=db.StringProperty(required=True)
    lastName=db.StringProperty(required=True)
    email=db.EmailProperty(required=True)
    password=db.StringProperty(required=True)
        
class Property(db.Model):
    title=db.StringProperty(required=True)
    description=db.TextProperty(required=False)
    availability=db.BooleanProperty(required=True)
    price=db.FloatProperty(required=True)
    nbedrooms=db.IntegerProperty(required=True)
    coord=db.GeoPtProperty(required=True)
    address=db.PostalAddressProperty(required=True)
    postalcode=db.StringProperty(required=True)
    city=db.StringProperty(required=True)
    state=db.StringProperty(required=False)
    country=db.StringProperty(required=True)
    images=db.ListProperty(item_type=blobstore.BlobKey,required=True)
    thumbnail=blobstore.BlobReferenceProperty(required=False)
    owner=db.IntegerProperty(required=True)
    rating=db.IntegerProperty(required=True)